# Restful api додаток для ведення футбольних груп.

## Реалізовано наступний функціонал:

1. Створення та видалення груп.
1. Додавання та видаленнякоманд у та з групи.
1. Генерація матчів у групі.
1. Зміна результату конкретного матчу.
1. Дані такі як групи, команди та результати ігор зберігаються в базі даних.
    
### Для побудови додатка використав:

* Backend - Laravel 5.8
* Frontend - VueJS ( + vue-route)
* Збереження даних - MySQL

### Список маршутів API:

	   GET | api/group                         | App\Http\Controllers\Groups@index </br>
	  POST | api/group                         | App\Http\Controllers\Groups@store </br>
	   GET | api/group/{group}                 | App\Http\Controllers\Groups@show  </br>
	DELETE | api/group/{group}                 | App\Http\Controllers\Groups@destroy </br>
	  POST | api/group/{group}/matches         | App\Http\Controllers\MatchesController@store </br>
	   PUT | api/group/{group}/matches/{match} | App\Http\Controllers\MatchesController@update </br>
	  POST | api/group/{group}/teams           | App\Http\Controllers\TeamsController@store </br>
	DELETE | api/group/{group}/teams/{team}    | App\Http\Controllers\TeamsController@destroy </br>

